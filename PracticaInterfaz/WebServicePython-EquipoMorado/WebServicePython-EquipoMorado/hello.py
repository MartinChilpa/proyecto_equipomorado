import PaginaWeb
from flask import Flask, render_template,jsonify, request
from pathlib import Path
#Comprobar si la base de datos existe.
f = Path('basemorada')
if f.exists() != True :
    PaginaWeb.INITDatabase("basemorada")




app = Flask(__name__)


@app.route("/consultar",methods=["GET"])

@app.route("/")    
def main2():
    return render_template('index.html');



@app.route("/main")    
def main():
    return render_template('index.html');


@app.route("/formulario")    
def formulario():
    return render_template('formulario.html');
    

    
@app.route("/respuesta",methods=['GET'])
def respuesta():
    ide= request.args.get("id")
    Url= request.args.get("url")
    Titulo= request.args.get("titulo")
    
    Pag= PaginaWeb.Pagina(ide,Url,Titulo)
    Pag.save()
    
    return render_template("registro_ingresado.html",ide=ide,Url=Url,Titulo=Titulo)
""""Página con el id "+request.args.get("id") + " registrado"

    """

@app.route("/id_search")    
def id_search():
    return render_template('buscador_id.html');
    

@app.route("/load",methods=['GET'])
def load ():
    ide=request.args.get("ide")
    print(ide)
    resultado = PaginaWeb.LOAD(ide)
    return render_template("respuesta_id.html",resultado=resultado)
    
    
@app.route("/mostrar_tabla")
def mostrar_tabla():
    resultado = PaginaWeb.Consultar_tabla()
    return render_template("mostrar_tabla.html",resultado=resultado)
    
@app.route("/nosotros")
def nosotros():
    return render_template("nosotros.html")


if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000,
       threaded=True, debug=True)

