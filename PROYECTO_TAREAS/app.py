from flask import Flask
from flask import request
from flask import render_template
from flask import session
from flask import redirect
from flask import url_for
from flask import send_from_directory,send_file
from io import BytesIO
#from flask_wtf.file import FileField
#from wtforms import SubmitField
#from flask_wtf import Form

from pathlib import Path
import os 
import Usuario
import Tareas
import Respuesta
import Solicitud
"""
f = Path('tareas.db')
if f.exists() != True :
    Usuario.INITDatabase("tareas.db")"""

app=Flask(__name__)    
app.secret_key="seguridadequipomorado"

@app.route('/favicon.ico') 
def favicon(): 
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='img/vnd.microsoft.icon')

@app.route("/")
def index():
    return render_template("index.html")
    
@app.route("/signin",methods=["POST","GET"])
def signin():
    if request.method == "POST":
        nocuenta=request.form["nocuenta"]
        nombres_sig=request.form["nombres"]
        correo=request.form["correo"]
        contrasenia=request.form["contrasenia"]
        
        usu=Usuario.Usuario(nombres_sig,nocuenta,correo,contrasenia)
        usu.guardar()
        return redirect(url_for("login"))
    else:
        return render_template("signin.html")



@app.route("/login", methods=["POST","GET"])
def login():
    
    if request.method == "POST":
        validacion1=Usuario.cargar(request.form["nocuenta"])
        nocuenta=request.form["nocuenta"]
        contrasenia=request.form["contrasenia"]
        print("contras",contrasenia)
        print("nocuneta",nocuenta)
        print(validacion1)
        if len(validacion1) != 0:
            if (nocuenta==validacion1[0]) and (contrasenia==validacion1[1]):
                print("bienvendo")
                #print(nocuenta)
                session["user"]=nocuenta
                return redirect(url_for("inicio"))
                #return render_template("inicio.html",nocuenta=nocuenta)
            else:
                if "user" in session:
                    return redirect(url_for("inicio"))
                    
                else:
                    print("Contraseña incorrecta")
                    return render_template("login.html",case=0)
                    #return redirect(url_for("login"))
        else:
            if "user" in session:
                return redirect(url_for("inicio"))
            else:
                #print("NO EXISTE REGISTREO")
                return render_template("login.html",case=1)
    else:
        if "user" in session:
             return redirect(url_for("inicio"))
        else:
            return render_template("login.html",case=3)
        
@app.route("/inicio")
def inicio():

    if "user" in session:
        nocuenta=session["user"]
        tareas=Tareas.tareas_disponibles(nocuenta)
        ids_tareas=Tareas.IDES_TAREAS(tareas)
        
       
        #ids_tareas=Tareas.lista_id()
        respuestas=Respuesta.respuestas_portarea(ids_tareas)
        longitud=Tareas.longitud_id(tareas)
        """
        print("Tareas",tareas)
        
        print("respuestas",respuestas)
        """
        print("ids_tareas",ids_tareas)
        print("respuestas",respuestas)
        print("LON",longitud)
        
        long_soli=Solicitud.long_solicitudes(nocuenta)
        print(long_soli)
    else:
        return redirect(url_for("index"))
    
    return render_template("inicio.html",nocuenta=nocuenta,tareas=tareas,respuestas=respuestas,longitud=longitud,long_soli=long_soli)
    
@app.route("/logout")
def logout():
    session.pop("user",None)
    return redirect(url_for("index"))
    
    
@app.route("/registroHW",methods=["POST","GET"])
def registroHW():

    if request.method == "POST":
        if "user" in session:
            nocuenta=session["user"]
    
        archivo=request.files["inputFile"]
        nombre_tar=request.form["nombre"]
        precio=request.form["precio"]
        materia=request.form["materia"]
        fecha=request.form["fecha"]
        hora=request.form["hora"]
        #archivo=request.form["archivo"]
        
        
        nombre_tar=archivo.filename
        data=archivo.read()
 
        tarea=Tareas.Tarea(nombre_tar,precio,materia,fecha,hora,nocuenta,data)
        tarea.guardar()
        #usu=Usuario.Usuario(nombres,nocuenta,correo,contrasenia)
        #usu.guardar()
        return render_template("confirmacion_registro.html",nombre_tar=nombre_tar,nocuenta=nocuenta)
    else:
        return render_template("registroHW.html")
        
@app.route("/tareas")        
def tareas():
    clave=request.args.get("id_tarea")
    lis=Tareas.Consultar_registro(clave)
    #print(clave)
   
    return render_template("tareas.html",lis=lis)

@app.route("/registroRES",methods=["POST","GET"])
def registroRES():
    
    datos=request.args.get("datos")
    datos=list(datos)
    datos=Respuesta.conversion(datos)
    
    if request.method == "POST":
        if "user" in session:
            nocuenta=session["user"]
            
        archivo=request.files["inputFile"]
        #Si el archivo es None se queda en la misma pagina
        if archivo.filename=="":
            return redirect(request.url)
        #Se obtiene NOMBRE,ID DE TAREA y BYTE INFO
        nombre=archivo.filename
        id_tarea=datos[4]
        data=archivo.read()
        nocuentat=datos[6]
        nombre=str(nombre)
        #Se guarda registro de tarea
        res=Respuesta.Respuesta(nombre,id_tarea,nocuenta,data)
        num=res.guardar()
        #print("Valor de res",(archivo))
        #print("nombre",nombre)
        #print("ide tarea",id_tarea)
        hora="wait"
        fecha="wait"
        ####PRUEBA
        ide_respuesta=Respuesta.retorno_ide(nocuenta,id_tarea)
        print("IDE_RESPUESTA",ide_respuesta)
        usuario=Usuario.cargar_usuario(nocuenta)
        print("USUARIOO",usuario)
        nom_usuario=usuario[0][1]
        nombre_tarea=datos[1]
        print("Dastossssssss",datos)
        
        
        solicitud=Solicitud.Solicitud(nocuenta,nocuentat,hora,fecha,nombre_tarea,nom_usuario,id_tarea,ide_respuesta)
        
        
        if num==0:
            solicitud.guardar()
            return render_template("confirmacion_res.html",nombre=nombre,nocuenta=nocuenta,datos=datos,usuario=usuario,ide_respuesta=ide_respuesta)
        else:
            return render_template("reg_denegado_res.html")
        #return archivo.filename
        # v.1return render_template("confirmacion_registro.html")
    else:
        return render_template("registroRES.html")


@app.route("/user")
def user():
    if "user" in session:
        nocuenta=session["user"]
        return f"<h1>{nocuenta}</h1>"
    else:
        return redirect(url_for("login"))
        
@app.route("/mis_tareas")
def mis_tareas():
    
    if "user" in session:
        
        prueba=request.args.get("ide")
        if prueba==None:
            pass
        else:
            Tareas.cambiar_estado(prueba)
            
            
        nocuenta=session["user"]
        a=Tareas.mis_tareas(nocuenta)
        id_tareas=Tareas.mis_tareas_id(nocuenta)
        respuestas=Respuesta.respuestas_portarea(id_tareas)
        
        longitud=Tareas.longitud_id(respuestas)
        
        #print("TAREAS",a)
        print("id_tareas",id_tareas)
        print("RESPUESTAS",respuestas)
        
    
        return render_template("mis_tareas.html",a=a,respuestas=respuestas,longitud=longitud,prueba=prueba)
    else:
        return redirect(url_for("index"))

      
@app.route("/download",methods=["POST","GET"])
def download():
    #b=Respuesta.download()
    data=request.args.get("datos")
    print("Ayuda")
    data=bytes(data)
    print(data)
    return send_file(BytesIO(data), attachment_filename='flask.pdf', as_attachment=True)
    #return(b)

@app.route("/download1",methods=["POST","GET"])
def download1():
    nombre=request.args.get("datos")
    print("NOMREE FROM TAREAS",nombre)
    b=Tareas.download(nombre)

    return b


@app.route("/download2",methods=["POST","GET"])
def download2():
    nombre=request.args.get("datos")
    print("NOMREE",nombre)
    b=Respuesta.download(nombre)
    
    return b
    #return(b)
    
@app.route("/mis_respuestas",methods=["POST","GET"])
def mis_respuestas():
    if "user" in session:
        nocuenta=session["user"]
  
        ide=request.args.get("id")
        usuario=request.args.get("usuario")
        print("usu",usuario)
        print("IDE",ide)
        print("tipos",type(ide))
        respuestas=Respuesta.respuestas_detareas(ide)
        print("RESPUESTAS",respuestas)
        
        return render_template("mis_respuestas.html",respuestas=respuestas)
    else:
        return render_template("inicio.html")
        
@app.route("/pagos",methods=["POST","GET"])
def pagos():
    
    if "user" in session:
        nocuenta=session["user"]
        ide_respuesta=request.args.get("id")
        print("IDE RESPUESTA",ide_respuesta)
        ide_tarea=Respuesta.respuestas_detareasporres(ide_respuesta)
        print(ide_respuesta)
        print("IDE TAREA",ide_tarea)
        
        #SE RECIBE ID_RESPUESTA
        #precio=ide_tarea[0][1]
        print(ide_tarea)

        precio=Tareas.precio_conide(ide_tarea[0][2])
        #precio=precio1[0][3]
        ide_tarea=ide_tarea[0][2]
        
        
        
        return render_template("pagos.html",nocuenta=nocuenta,ide_respuesta=ide_respuesta,ide_tarea=ide_tarea,precio=precio)
    else:
        return render_template("index.html")
        
        
@app.route("/terminosycondiciones")
def terminosycondiciones():
    return render_template("terminos.html")
    
@app.route("/solicitudes")
def solicitudes():
     if "user" in session:
        nocuenta=session["user"]
        
        lis=Solicitud.solicitudes_lista(nocuenta)
        
        return render_template("solicitudes.html",lis=lis)
    

"""    
@app.route("/download2",methods=["POST","GET"])
def download2():
    #b=Respuesta.download()
    
    data=request.args.get("datos")
    
    print("ESTOO",data)
    return (f"<h1>{data}</h1>")
    




@app.route("/argsHTML")    
@app.route("/argsHTML/<nombre>")
def argsHTML(nombre="DEFAULT"):
    lista=["hola","deja","de","hacer","buches"]
    age=10
    return render_template("user.html",nombre=nombre,age=age,lista=lista)
    
    #http://100.24.37.71:5000/params?params1=Martin

@app.route("/params")
def params():
    param=request.args.get("params1","No parametro")
    #eturn "El parametro es {}".format(param)
    return "El parametro es "+param


@app.route("/params")    
@app.route("/params/<name>")
@app.route("/params/<name>/<int:last>")
def params(name="valo default",last="sinapellido"):
    return "El parametro es {} {} ".format(name,last)
    """

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000,
       threaded=True, debug=True)