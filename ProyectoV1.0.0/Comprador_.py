"""
clase Comprador-Hereda de usuario
"""
import Usuario_
#import Tarea
#import Respuesta

import sqlite3

listaComprador=[]
listaCalificaciones=[]


#Definimos la clase Comprador que va a heredar de usuario

class Comprador(Usuario_.Usuario):
    
    
    def __init__(self,nombre,no_cuenta,correo):
        #Atributos
        #Reputacion: variable de tipo float, valor maximo 5
        self.reputacion=0
        #self.lista_tareas_hechas=lista_tareas_hechas
        super().__init__(nombre,no_cuenta,correo)
        #self.lista_tareas_hechas=lista_tareas_hechas
        self.lista_tareas_hechas=[]
        listaComprador.append(self)
        self.no_cuenta=no_cuenta
        self.nombre=nombre
        self.monedero=0
        self.listaCalificaciones=[]
        
    def __str__(self):
        return str(self.reputacion)+' '+self.nombre+' '+self.correo+' '+str(self.monedero)
        
    
    def AgregarRegistro(self):
        conexion=sqlite3.connect("BaseMorada")
        cur=conexion.cursor()
        cur.execute("SELECT * FROM Comprador")
        seleccion=cur.fetchall()
        
        seleccion_long=len(seleccion)
        
   
        no_cuenta_Lista=[]
        
        for i in range(0,seleccion_long):
            registro=int((seleccion[i][0]))
            no_cuenta_Lista.append(registro)
            
        #print(no_cuenta_Lista)
            
        
        if seleccion_long==0:
            print(self.no_cuenta, "es el primer registrooo")
            cur.execute("INSERT INTO VENDEDOR (NO_CUENTA,CORREO,NOMBRE_VENDEDOR) VALUES('{}','{}','{}');".format(self.no_cuenta,self.correo,self.nombre))
        
        else:
            existencia=0
        
            if self.no_cuenta in no_cuenta_Lista:
                existencia=1
                print(self.no_cuenta,"Ya existe")
            
            if existencia==1:
                print("Lo sentimos, esa cuenta ya existe",self.no_cuenta)
                #cur.execute("UPDATE VENDEDOR SET CORREO='{}',NOMBRE_VENDEDOR='{}'".format(self.correo, self.nombre))
            
            else:
                print("Registrando...",self.no_cuenta)
                cur.execute("INSERT INTO VENDEDOR (NO_CUENTA,CORREO,NOMBRE_VENDEDOR) VALUES('{}','{}','{}');".format(self.no_cuenta,self.correo,self.nombre))
                
        
        
        """
        id_list=[]
        
        for i in range(0,len(seleccion)):
            registro=int(seleccion[i][0])
            id_list.append(registro)
            
        if int(self.no_cuenta) in id_list:
            print("Registro existente, actualizando...")
            cur.execute("UPDATE COMPRADOR SET CORREO='{}',NOMBRE_COMPRADOR='{}'".format(self.correo, self.nombre))
            
        else:
            print("Registrando........")
            cur.execute("INSERT INTO COMPRADOR (NO_CUENTA,CORREO,NOMBRE_COMPRADOR) VALUES('{}','{}','{}');".format(self.no_cuenta,self.correo,self.nombre))
            """
        conexion.commit()
        conexion.close()



def Consultar_tablaComprador():
    tablas=[]
    
    conexion=sqlite3.connect("BaseMorada")
    cur=conexion.cursor()
    cur.execute("SELECT * FROM Comprador;")
    seleccion=cur.fetchall()
    
    print("TABLA COMPRADOR")
    for i in seleccion:
        tablas.append(i)
        print(i)
    return tablas
    conexion.commit()
    conexion.close()
